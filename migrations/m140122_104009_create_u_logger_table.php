<?php

class m140122_104009_create_u_logger_table extends EDbMigration
{
	public function safeUp()
	{
                $this->createTable('u_logger', array(
                        'id'=>'pk',
                        'active_user_id'=>'int',
                        'is_console'=>'tinyint(1) not null',
                        'category'=>'int not null',
                        'create_date'=>'int not null',
                        'name'=>'varchar(255) not null',
                        'ip'=>'varchar(255) not null',
                        'body'=>'text not null',
                        'php_server'=>'text not null',
                        'php_request'=>'text not null',
                        'php_session'=>'text not null',
                        'php_cookies'=>'text not null',
                ));
	}

	public function safeDown()
	{
                $this->dropTable('u_logger');
	}
}
