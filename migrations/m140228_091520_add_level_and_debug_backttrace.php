<?php

class m140228_091520_add_level_and_debug_backttrace extends EDbMigration
{
	public function safeUp()
	{
                $this->addColumn('u_logger', 'debug_backtrace', 'text not null');
                $this->renameColumn('u_logger', 'category', 'level');
                $this->addColumn('u_logger', 'category', 'varchar(255) not null');

                Yii::app()->cache->flush();
	}

	public function safeDown()
	{
                $this->dropColumn('u_logger', 'category');
                $this->dropColumn('u_logger', 'debug_backtrace');
                $this->renameColumn('u_logger', 'level', 'category');

                Yii::app()->cache->flush();
	}
}
