<?php

class m140228_105344_clear_old_logs extends EDbMigration
{
	public function safeUp()
	{
                $this->truncateTable('u_logger');
	}

	public function safeDown()
	{
	}
}
