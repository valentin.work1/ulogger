<?php $this->breadcrumbs = array(
        "Управление"=>array("admin"),
        "Детали"
); ?>

<h2><?php echo $model->name; ?></h2>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
                array(
                        'name'=>'is_console',
                        'value'=>ULogger::getConsoleValue($model->is_console),
                ),
                'category',
                array(
                        'name'=>'level',
                        'value'=>ULogger::getLevelValue($model->level),
                        'type'=>'raw',
                ),
                array(
                        'name'=>'create_date',
                        'value'=>date('Y-m-d H:i:s', $model->create_date),
                ),
                array(
                        'name'=>'active_user_name',
                        'value'=>CHtml::value($model->user, "username"),
                ),
		'ip',
		'name',
                array(
                        'name'=>'body',
                        'value'=>nl2br($model->body),
                        'type'=>'raw',
                        'cssClass'=>'alert alert-info',
                ),
                array(
                        'name'=>'debug_backtrace',
                        'value'=>nl2br($model->debug_backtrace),
                        'type'=>'raw',
                ),
                array(
                        'name'=>'php_server',
                        'value'=>nl2br($model->php_server),
                        'type'=>'raw',
                ),
                array(
                        'name'=>'php_request',
                        'value'=>nl2br($model->php_request),
                        'type'=>'raw',
                ),
                array(
                        'name'=>'php_session',
                        'value'=>nl2br($model->php_session),
                        'type'=>'raw',
                ),
                array(
                        'name'=>'php_cookies',
                        'value'=>nl2br($model->php_cookies),
                        'type'=>'raw',
                ),
	),
        'htmlOptions'=>array('class'=>'table table-condensed table-striped table-hover table-bordered table-side'),
)); ?>

<br><br><br><br>
