<?php $this->breadcrumbs = array("Логи"); ?>

<?php $pageSize = Yii::app()->user->getState("pageSize",20); ?>
<h2>Логи</h2>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ulogger-grid',
	'dataProvider'=>$model->search(),
        'ajaxUpdate'=>false,
	'filter'=>$model,
	'columns'=>array(
                array(
                        'name'=>'id',
                        'htmlOptions'=>array(
                                'width'=>'25',
                                'class'=>'centered'
                        ),
                ),
                array(
                        'name'=>'name',
                        'value'=>'CHtml::link($data->name, array("view", "id"=>$data->id))',
                        'type'=>'raw',
                ),
                array(
                        'name'=>'active_user_name',
                        'value'=>'CHtml::value($data->user, "username")',
                ),
                array(
                        'name'=>'category',
                        'filter'=>ULogger::getCategoryList(),
                ),
                array(
                        'name'=>'level',
                        'filter'=>ULogger::getLevelList(),
                        'value'=>'ULogger::getLevelValue($data->level)',
                        'type'=>'raw',
                ),
                array(
                        'name'=>'create_date',
                        'value'=>'date("Y-m-d H:i:s", $data->create_date)',
                ),
		'ip',
                array(
                        'name'=>'is_console',
                        'filter'=>ULogger::getConsoleList(),
                        'value'=>'ULogger::getConsoleValue($data->is_console)',
                        'type'=>'raw',
                ),
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{view}',
                        'header'=>CHtml::dropDownList('pageSize',$pageSize,array(20=>20,50=>50,100=>100,200=>200),array(
                                'onchange'=>"$.fn.yiiGridView.update('ulogger-grid',{ data:{pageSize: $(this).val() }})",
                                'style'=>'width:50px'
                        )),
		),
	),
        'itemsCssClass'=>'table table-hover table-striped table-bordered table-condensed',
)); ?>

