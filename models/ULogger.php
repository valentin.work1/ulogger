<?php

/**
 * This is the model class for table "u_logger".
 *
 * The followings are the available columns in table 'u_logger':
 * @property integer $id
 * @property integer $active_user_id
 * @property integer $is_console
 * @property integer $category
 * @property integer $create_date
 * @property string $name
 * @property string $ip
 * @property string $body
 * @property string $php_server
 * @property string $php_request
 * @property string $php_session
 * @property string $php_cookies
 */
class ULogger extends CActiveRecord
{
        CONST ERROR = 0;
        CONST INFO = 1;

        /**
         * For search in admin action 
         * 
         * @var string
         */
        public $active_user_name;


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'u_logger';
	}

	public function rules()
	{
		return array(
			array('id, active_user_id, level, is_console, category, create_date, name, ip, body, active_user_name', 'safe', 'on'=>'search'),
		);
	}


        public function purgeXSS($attr)
        {
                $this->$attr = htmlspecialchars($this->$attr, ENT_QUOTES);
                return true;
        }


	public function relations()
	{
		return array(
                        'user'=>array(self::BELONGS_TO, 'User', 'active_user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'active_user_id' => 'Active User',
			'is_console' => 'Console',
			'category' => 'Category',
			'create_date' => 'Create Date',
			'name' => 'Name',
			'ip' => 'Ip',
			'body' => 'Body',
			'php_server' => 'Php Server',
			'php_request' => 'Php Request',
			'php_session' => 'Php Session',
			'php_cookies' => 'Php Cookies',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

                $criteria->with = array('user');
		$criteria->compare('t.id',$this->id);
		$criteria->compare('user.username',$this->active_user_name, true);
		$criteria->compare('t.is_console',$this->is_console);
		$criteria->compare('t.category',$this->category);
		$criteria->compare('t.level',$this->level);
		$criteria->compare('t.create_date',$this->create_date);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.ip',$this->ip,true);

		return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'sort'=>array(
                                'defaultOrder'=>'t.id DESC',
                        ),
                        'pagination'=>array(
                                'pageSize'=>Yii::app()->user->getState('pageSize',20), 
                        ),
		));
	}

        //=========== Lists ===========

        /**
         * getCategoryList 
         * 
         * @return array
         */
        public static function getCategoryList()
        {
                $criteria = new CDbCriteria;
                $criteria->select = 'category';
                $criteria->group = 'category';

                return CHtml::listData(ULogger::model()->findAll($criteria), 'category', 'category');
        }

        /**
        * getCategoryList 
        * 
        * @return array
        */
        public static function getLevelList($withSpan = false)
        {
                if ($withSpan) 
                {
                        return array(
                                0 => "<span class='label label-important'>Error</span>",
                                1 => "<span class='label label-info'>Info</span>",
                                2 => "<span class='label'>Dirty</span>",
                        );
                } 
                else 
                {
                        return array(
                                0 => 'Error',
                                1 => 'Info',
                                2 => 'Dirty',
                        );
                }
        }
        
        /**
        * getLevelValue 
        * 
        * @param string $val 
        * @return string
        */
        public static function getLevelValue($val)
        {
                $ar = self::getLevelList(true);
                return isset($ar[$val]) ? $ar[$val] : $val;
        }

        /**
        * getConsoleList 
        * 
        * @return array
        */
        public static function getConsoleList()
        {
                return array(
                        0 => 'Нет',
                        1 => 'Да',
                );
        }
        
        /**
        * getConsoleValue 
        * 
        * @param string $val 
        * @return string
        */
        public static function getConsoleValue($val)
        {
                $ar = self::getConsoleList();
                return isset($ar[$val]) ? $ar[$val] : $val;
        }

        //----------- Lists -----------

        /**
         * saveLog 
         * 
         * @param string $name 
         * @param string $body 
         * @param int $level 
         * @param string $category 
         * @return void
         */
        public static function saveLog($name, $body = '', $level = self::ERROR, $category = 'default')
        {
                $model = new self;

                $model->name        = $name;
                $model->body        = $body;
                $model->level       = $level;
                $model->category    = $category;
                $model->create_date = time();

                // Console
                if (php_sapi_name() == 'cli') 
                {
                        $model->is_console = 1;
                } 
                else 
                {
                        $backtrace = debug_backtrace();

                        $model->is_console      = 0;
                        $model->active_user_id  = Yii::app()->user->isGuest ? null : Yii::app()->user->id;
                        $model->php_server      = isset($_SERVER) ? print_r($_SERVER, true) : 'No server data';
                        $model->php_request     = isset($_REQUEST) ? print_r($_REQUEST, true) : 'No request data';
                        $model->php_session     = isset($_SESSION) ? print_r($_SESSION, true) : 'No session data';
                        $model->php_cookies     = isset($_COOKIE) ? print_r($_COOKIE, true) : 'No cookies data';
                        $model->debug_backtrace = isset($backtrace[1]) ? print_r($backtrace[1], true) : 'No debug backtrace';
                        $model->ip              = self::getRealIp();
                }

                $model->save(false);

        }

        /**
         * getRealIp 
         * 
         * @return string
         */
        public static function getRealIp()
        {
                $ip = '';

                $client  = @$_SERVER['HTTP_CLIENT_IP'];
                $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                $remote  = @$_SERVER['REMOTE_ADDR'];

                if(filter_var($client, FILTER_VALIDATE_IP))
                {
                        $ip = $client;
                }
                elseif(filter_var($forward, FILTER_VALIDATE_IP))
                {
                        $ip = $forward;
                }
                else
                {
                        $ip = $remote;
                }

                return $ip; 
        }
}
