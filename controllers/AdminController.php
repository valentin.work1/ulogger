<?php

class AdminController extends AdminDefaultController
{
        public $modelName = 'ULogger';
        public $layout = '//layouts/back';

        public $noPages = array('index','create', 'update', 'redirect'=>array('admin'));
        public $createRedirect = array('admin');
        public $updateRedirect = 'goBack';

}
